﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FC.Data
{
    public class SubjectMatter
    {
        public string id { get; set; }
        public string itemUrl { get; set; }
        public string status { get; set; }
        public string title { get; set; }
        public string picUrl { get; set; }
        public string initialPrice { get; set; }
        public string currentPrice { get; set; }
        public string consultPrice { get; set; }
        public string marketPrice { get; set; }
        public string sellOff { get; set; }
        public string start { get; set; }
        public string end { get; set; }
        public string timeToStart { get; set; }
        public string timeToEnd { get; set; }
        public string viewerCount { get; set; }
        public string bidCount { get; set; }
        public string delayCount { get; set; }
        public string applyCount { get; set; }
        public string xmppVersion { get; set; }
        public string buyRestrictions { get; set; }
        public string supportLoans { get; set; }
        public string supportOrgLoan { get; set; }
    }
}
