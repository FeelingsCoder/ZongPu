﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApp.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Test()
        {
            return View();
        }

        public JsonResult UploadFile()
        {
            return Json("ok");
        }

        public JsonResult DeleteFile(string key, Props extra)
        {
            return Json("{}");
        }
    }

    public class Props
    {
        public string id { get; set; }
    }
}