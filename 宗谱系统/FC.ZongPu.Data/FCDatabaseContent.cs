﻿using FC.ZongPu.Data.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FC.ZongPu.Data
{
    /// <summary>
    /// 数据库上下文
    /// </summary>
    public class FCDatabaseContent : DbContext
    {
        /// <summary>
        /// 用户
        /// </summary>
        public DbSet<User> Users { get; set; }
        /// <summary>
        /// 人员
        /// </summary>
        public DbSet<People> Peoples { get; set; }

        /// <summary>
        /// 配偶
        /// </summary>
        public DbSet<Spouse> Spouses { get; set; }

        #region 加载实体映射
        /// <summary>
        /// 加载实体映射
        /// </summary>
        /// <param name="modelBuilder"></param>
		protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Properties().Where(p => p.Name == "RowVersion" || p.Name == "RowVersion".ToUpper()).Configure(p => p.IsConcurrencyToken());
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();  //表中都统一设置禁用一对多级联删除
            modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>(); //表中都统一设置禁用多对多级联删除
            base.OnModelCreating(modelBuilder);
        }
        #endregion
    }
}
