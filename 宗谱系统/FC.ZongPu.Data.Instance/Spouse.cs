﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FC.ZongPu.Data.Models
{
    /// <summary>
    /// 配偶表
    /// </summary>
    public class Spouse
    {
        /// <summary>
        /// 配偶编号
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// 外键：丈夫编号
        /// </summary>
        public string HusbandId { get; set; }

        /// <summary>
        /// 导航属性：丈夫
        /// </summary>
        public People Husband { get; set; }

        /// <summary>
        /// 外键：妻子编号
        /// </summary>
        public string WifeId { get; set; }

        /// <summary>
        /// 导航属性：妻子
        /// </summary>
        public People Wife { get; set; }
    }
}
