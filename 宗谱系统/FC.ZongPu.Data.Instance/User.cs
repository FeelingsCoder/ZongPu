﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FC.ZongPu.Data.Models
{
    public class User
    {
        /// <summary>
        /// 用户编号
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// 用户名
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// 密码
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// 外键：人员信息编号
        /// </summary>
        public string PeopleId { set; get; }

        /// <summary>
        /// 导航属性：人员信息
        /// </summary>
        public People People { get; set; }
    }
}
