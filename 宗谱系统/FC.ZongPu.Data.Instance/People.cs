﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FC.ZongPu.Data.Models
{
    public class People
    {
        /// <summary>
        /// 人员编号
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// 姓名
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 性别
        /// </summary>
        public string Sex { get; set; }

        /// <summary>
        /// 生日
        /// </summary>
        public DateTime Birthday { get; set; }

        /// <summary>
        /// 辈分
        /// </summary>
        public int Seniority { get; set; }

        /// <summary>
        /// 外键：父亲的编号
        /// </summary>
        public string FatherId { get; set; } 

        /// <summary>
        /// 导航属性：父亲
        /// </summary>
        public People Father { get; set; }

        /// <summary>
        /// 外键：母亲的编号
        /// </summary>
        public string MotherId { get; set; }

        /// <summary>
        /// 导航属性：母亲
        /// </summary>
        public People Mother { get; set; }
    }
}
